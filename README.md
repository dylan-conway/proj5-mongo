# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

Author: Dylan Conway Email: dconway@uoregon.edu

## What is in this repository

This is a website that will calculate times and store them in a mongo database. The times can be displayed in a separate page. To store the times, all you need to do is click the submit button which will send the times over to the server.